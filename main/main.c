#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/ledc.h"
#include "driver/i2s.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "esp_task_wdt.h"
#include "esp_spiffs.h"
#include "driver/rtc_io.h"
#include "esp_partition.h"
#include "esp_ota_ops.h"

#include "../components/odroid/odroid_util.h"
#include "../components/odroid/odroid_settings.h"
#include "../components/odroid/odroid_input.h"
#include "../components/odroid/odroid_display.h"
#include "../components/odroid/odroid_audio.h"
#include "../components/odroid/odroid_system.h"
#include "../components/odroid/odroid_print.h"
#include "../components/odroid/odroid_scandir.h"

#include "../components/spectrum/compr.c"
#include "../components/spectrum/interf.c"
#include "../components/spectrum/keynames.c"
#include "../components/spectrum/loadim.c"
#include "../components/spectrum/misc.c"
//#include "../components/spectrum/rom_imag.c"
#include "../components/spectrum/snapshot.c"
#include "../components/spectrum/spconf.c"
#include "../components/spectrum/spect.c"
#include "../components/spectrum/spectkey.c"
#include "../components/spectrum/spkey.c"
#include "../components/spectrum/spmain.c"
#include "../components/spectrum/spperif.c"
#include "../components/spectrum/spscr.c"
#include "../components/spectrum/spsound.c"
#include "../components/spectrum/sptape.c"
#include "../components/spectrum/sptiming.c"
#include "../components/spectrum/stubs.c"
#include "../components/spectrum/tapefile.c"
#include "../components/spectrum/vgakey.c"
#include "../components/spectrum/vgascr.c"
#include "../components/spectrum/z80.c"
#include "../components/spectrum/z80_op1.c"
#include "../components/spectrum/z80_op2.c"
#include "../components/spectrum/z80_op3.c"
#include "../components/spectrum/z80_op4.c"
#include "../components/spectrum/z80_op5.c"
#include "../components/spectrum/z80_op6.c"
#include "../components/spectrum/z80optab.c"
#include "../components/spectrum/z80_step.c"

#include <string.h>
#include <dirent.h>

extern int debug_trace;

int keyboard = 0; // on-screen keyboard active?
int b_up = 6, b_down = 5, b_left = 4, b_right = 7;
int b_a = 9, b_b = 8, b_select = 39, b_start = 29; // 8 button maps

char key[40][6] = {
    "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
    "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
    "a", "s", "d", "f", "g", "h", "j", "k", "l", "enter",
    "caps", "z", "x", "c", "v", "b", "n", "m", "symbl", "space"};

odroid_gamepad_state joystick;

odroid_battery_state battery_state;

#define AUDIO_SAMPLE_RATE (15600) // 624 samples/frame at 25fps

int BatteryPercent = 100;

uint16_t *menuFramebuffer = 0;

unsigned short *buffer;   // screen buffer.
struct dirent **namelist; // directory scan buffer
int names_count = 0;      // number of records in **namelist(directory cache)

char path[256] = "/sd/roms/spectrum", file[256] = "/sd/roms/spectrum/basic.sna", path_scanned[256] = "";

//----------------------------------------------------------------
void read_resume()
{
  FILE *fp;

  if ((fp = fopen("/sd/roms/spectrum/resume.txt", "r")))
  {
    fgets(path, sizeof path, fp);
    path[strlen(path) - 1] = 0;
    fgets(file, sizeof file, fp);
    file[strlen(file) - 1] = 0;
    fclose(fp);

    printf("read_resume: path='%s'\n", path);
    printf("read_resume: file='%s'\n", file);
   }
}

//----------------------------------------------------------------
void save_resume()
{
  FILE *fp;

  if ((fp = fopen("/sd/roms/spectrum/resume.txt", "w")))
  {
    fprintf(fp, "%s\n", path);
    fprintf(fp, "%s\n", file);
    fclose(fp);

    // printf("save_resume: path='%s'\n", path);
    // printf("save_resume: file='%s'\n", file);
  }
}

//----------------------------------------------------------------
void read_buttons()
{
  FILE *fp;

  char *fileName = odroid_util_GetButtonsPath(file);
  char line[256];

  if (strlen(fileName) > 0)
  {
    //printf("read_buttons: fileName: '%s', length: %d\n", fileName, strlen(fileName));

    if ((fp = fopen(fileName, "r")))
    {
      printf("read_buttons: '%s' found\n", fileName);
      fgets(line, 256, fp); // skip the header
      fscanf(fp, "%i,%i,%i,%i,%i,%i,%i,%i\n",
             &b_up, &b_down, &b_left, &b_right, &b_a, &b_b, &b_select, &b_start);
      fclose(fp);
    }
    else
    {
      printf("read_buttons: '%s' not found\n", fileName);
    }
    printf("read_buttons: UP: %s, DOWN: %s, LEFT: %s, RIGHT: %s, A: %s, B: %s, SELECT: %s, START: %s\n",
           key[b_up], key[b_down], key[b_left], key[b_right], key[b_a], key[b_b], key[b_select], key[b_start]);
  }
  else
  {
    printf("read_buttons: warning file '%s' is not a rom file, not reading the buttons\n", file);
  }
  free_string(fileName);
}

//----------------------------------------------------------------
void save_buttons()
{
  FILE *fp;

  char *fileName = odroid_util_GetButtonsPath(file);
  if (strlen(fileName) > 0)
  {
    printf("save_buttons() to '%s' found\n", fileName);

    if ((fp = fopen(fileName, "w")))
    {
      fprintf(fp, "UP,DOWN,LEFT,RIGHT,A,B,SELECT,START\n"); // Add header fro reference when editiing this file manually
      fprintf(fp, "%i,%i,%i,%i,%i,%i,%i,%i\n",
              b_up, b_down, b_left, b_right, b_a, b_b, b_select, b_start);
      fclose(fp);
    }
  }
  else
  {
    printf("save_buttons: warning file '%s' is not a rom file, not saving the buttons\n", file);
  }
  free_string(fileName);
}

//----------------------------------------------------------------
void load_spectrum_files(int save)
{
  if (odroid_util_IsRomFile(file) == 1)
  {
    load_snapshot_file_type(file, -1);
    read_buttons();

    if (save == 1)
    {
      save_resume();
    }
  }
  else
  {
    printf("load_spectrum_files: Not a ROM file selected, doing nothing\n");
  }
}

//----------------------------------------------------------------
void debounce(int key)
{
  while (joystick.values[key])
    odroid_input_gamepad_read(&joystick);
}

//----------------------------------------------------------------
void clean_cache_rom_scandir()
{
  if (names_count == 0)
  {
    //printf("clean_cache_rom_scandir: nothing to do\n");
    return;
  }

  //printf("clean_cache_rom_scandir: cleaning memory\n");

  // clear memory before scan;
  int count = 0;
  while (count < names_count)
  {
    //printf("rom_scandir:  free mem, i=%i, file: %s\n", count, namelist[count]->d_name);
    free(namelist[count]);
    count++;
  }
  free(namelist);
  names_count = 0;
}

//----------------------------------------------------------------
void rom_scandir(char *path)
{
  if (names_count > 0)
  {
    //printf("rom_scandir: no need to re-scan a directory\n");
    return;
  }

  //printf("rom_scandir\n");
  clean_cache_rom_scandir();

  strcpy(path_scanned, path);

  names_count = odroid_Scandir(path, &namelist);
  if (names_count < 0)
  {
    printf("rom_scandir: error: (names_count < 0)\n");
  }
  else
  {
    //printf("rom_scandir: read %i items\n", names_count);
  }
}

//---------------------------------------------------------------
int choose_file()
{
  struct dirent *de;
  int y = 0, count, redraw = 0, len;
  DIR *dr;

  de = malloc(sizeof(struct dirent));

  while (1)
  { // stay in file chooser until something loaded or menu button pressed
    redraw = 0;
    count = 0;
    ili9341_clear(0); // clear screen
    printx2(5, 0, "Load Snapshot");
    print(0, 3, path);
    printf("choose_file: while (1), y=%d, count=%d, path='%s'\n", y, count, path);

    if (strcmp(path_scanned, path) != 0) // if we are in a new path then clean cache and rescan the directory the next time
    {
      clean_cache_rom_scandir();
    }
    rom_scandir(path);

    if (names_count < 0)
    {
      printf("choose_file: error: (names_count < 0)\n");
    }
    else
    {
      char name[35];
      name[34] = 0;

      while (count < names_count)
      {
        //printf("choose_file: i=%i, file: %s\n", count, namelist[count]->d_name);

        if (count / 20 == y / 20)
        {
          strncpy(name, namelist[count]->d_name, 34);

          print(1, (count % 20) + 5, name);
          if (namelist[count]->d_type == DT_DIR)
            print(1 + strlen(name), (count % 20) + 5, "/");
        }
        count++;
      }
    }

    print(0, (y % 20) + 5, ">");

    // process key presses...
    while (!joystick.values[ODROID_INPUT_A] &&
           !joystick.values[ODROID_INPUT_B] &&
           !redraw)
    {
      odroid_input_gamepad_read(&joystick);
      if (joystick.values[ODROID_INPUT_UP])
      {
        debounce(ODROID_INPUT_UP);
        print(0, (y % 20) + 5, " ");
        if (y > 0)
        {
          y--;
          if (y % 20 == 19)
            redraw = 1;
        }
        else
        {
          y = count - 1;
          redraw = 1;
        }
        print(0, (y % 20) + 5, ">");
      }
      if (joystick.values[ODROID_INPUT_DOWN])
      {
        debounce(ODROID_INPUT_DOWN);
        print(0, (y % 20) + 5, " ");
        if (y < count - 1)
        {
          y++;
          if (y % 20 == 0)
            redraw = 1;
        }
        else
        {
          y = 0;
          redraw = 1;
        }
        print(0, (y % 20) + 5, ">");
      }
      if (joystick.values[ODROID_INPUT_LEFT])
      {
        debounce(ODROID_INPUT_LEFT);
        print(0, (y % 20) + 5, " ");
        y -= 20;
        if (y < 0)
          y = 0;
        print(0, (y % 20) + 5, ">");
        redraw = 1;
      }
      if (joystick.values[ODROID_INPUT_RIGHT])
      {
        debounce(ODROID_INPUT_RIGHT);
        print(0, (y % 20) + 5, " ");
        y += 20;
        if (y > count - 1)
          y = count - 1;
        print(0, (y % 20) + 5, ">");
        redraw = 1;
      }
      if (joystick.values[ODROID_INPUT_MENU])
      { // just exit menu...
        debounce(ODROID_INPUT_MENU);
        return (0);
      }
    }
    // OK, either 'A' pressed or screen re-draw needed....
    if (joystick.values[ODROID_INPUT_A] && count == 0) // empty directory
      debounce(ODROID_INPUT_A);
    else if (joystick.values[ODROID_INPUT_A])
    {
      debounce(ODROID_INPUT_A);

      count = 0;
      //printf("choose_file: Entering new directory, names_count=%i,count=%i\n", names_count, count);
      if (names_count < 0)
      {
        printf("choose_file: error 2: (names_count < 0)\n");
      }
      else
      {
        while (count < names_count)
        {
          //printf("choose_file: check selected dir/file i=%i, file: %s\n", count, namelist[count]->d_name);

          if (count == y)
          {
            //printf("choose_file: select dir/file i=%i, file: %s\n", count, namelist[count]->d_name);

            de->d_type = namelist[count]->d_type;
            de->d_ino = namelist[count]->d_ino;
            strcpy(de->d_name, namelist[count]->d_name);
          }
          count++;
        }
      }

      if (de->d_type == DT_DIR)
      { // go into directory...
        //printf("choose_file: Entering new directory\n");
        len = strlen(path);
        path[len] = '/';
        strcat(&path[len + 1], de->d_name);
        y = 0; // go to first option
      }
      if (de->d_type == DT_REG)
      { // file....
        file[0] = 0;
        strcat(file, path);
        strcat(file, "/");
        strcat(file, de->d_name);

        load_spectrum_files(1); // load snapshot and button mapping, and save the resume.txt file with the new file
        ili9341_clear(0); // clear screen
        return (0);
      }
    }
    if (joystick.values[ODROID_INPUT_B])
    { // up a directory?
      debounce(ODROID_INPUT_B);
      y = 0; // back to first item in list
      if (strlen(path) > 3)
      { // safe to go up a directory...
        while (path[strlen(path) - 1] != '/')
          path[strlen(path) - 1] = 0;
        path[strlen(path) - 1] = 0;
      }
    }
  }
}

//----------------------------------------------------------------
void save()
{
  char *fileSave = odroid_util_GetSavePath(file); // switch to the "save" file, so we always save to a copy instead of the original rom
  strcpy(file, fileSave);
  free_string(fileSave);

  printx2(12, 25, "SAVING");
  printf("save: ='%s'\n", file);
  save_snapshot_file(file);
  save_resume();
  clean_cache_rom_scandir(); // Clear the cache, so if we use the "Load Snapshot" we see the new snapshot in the directory list when we save it for the first time
  printx2(12, 25, "SAVED.");
  sleep(1);
}

//----------------------------------------------------------------
void draw_keyboard()
{
  print(0, 24, "                                     ");
  print(0, 25, "      1  2  3  4  5  6  7  8  9  0   ");
  print(0, 26, "      q  w  e  r  t  y  u  i  o  p   ");
  print(0, 27, "      a  s  d  f  g  h  j  k  l ent  ");
  print(0, 28, "     cap z  x  c  v  b  n  m sym _   ");
  print(0, 29, "                                     ");

  kb_set(); // add cursor
}

//---------------------------------------------------------------
void kb_blank()
{
  if (kbpos == 29 || kbpos == 30 || kbpos == 38)
  {
    print(4 + (kbpos % 10) * 3, 25 + (kbpos / 10), " ");
    print(8 + (kbpos % 10) * 3, 25 + (kbpos / 10), " ");
  }
  else
  {
    print(5 + (kbpos % 10) * 3, 25 + (kbpos / 10), " ");
    print(7 + (kbpos % 10) * 3, 25 + (kbpos / 10), " ");
  }
}

void kb_set()
{
  if (kbpos == 29 || kbpos == 30 || kbpos == 38)
  {
    print(4 + (kbpos % 10) * 3, 25 + (kbpos / 10), ">");
    print(8 + (kbpos % 10) * 3, 25 + (kbpos / 10), "<");
  }
  else
  {
    print(5 + (kbpos % 10) * 3, 25 + (kbpos / 10), ">");
    print(7 + (kbpos % 10) * 3, 25 + (kbpos / 10), "<");
  }
}

int get_key(int current)
{
  draw_keyboard();
  while (1)
  {
    odroid_input_gamepad_read(&joystick);

    if (joystick.values[ODROID_INPUT_UP])
    {
      kb_blank();
      kbpos -= 10;
      if (kbpos < 0)
        kbpos += 40;
      kb_set();
      debounce(ODROID_INPUT_UP);
    }
    if (joystick.values[ODROID_INPUT_DOWN])
    {
      kb_blank();
      kbpos += 10;
      if (kbpos > 39)
        kbpos -= 40;
      kb_set();
      debounce(ODROID_INPUT_DOWN);
    }

    if (joystick.values[ODROID_INPUT_LEFT])
    {
      kb_blank();
      kbpos--;
      if (kbpos % 10 == 9 || kbpos == -1)
        kbpos += 10;
      kb_set();
      debounce(ODROID_INPUT_LEFT);
    }
    if (joystick.values[ODROID_INPUT_RIGHT])
    {
      kb_blank();
      kbpos++;
      if (kbpos % 10 == 0)
        kbpos -= 10;
      kb_set();
      debounce(ODROID_INPUT_RIGHT);
    }
    if (joystick.values[ODROID_INPUT_A])
    {
      debounce(ODROID_INPUT_A);
      return (kbpos);
    }
    if (joystick.values[ODROID_INPUT_B])
    {
      debounce(ODROID_INPUT_B);
      return (current);
    }
    if (joystick.values[ODROID_INPUT_MENU])
    {
      debounce(ODROID_INPUT_MENU);
      return (current);
    }
  }
}
//-----------------------------------------------------------------
// screen for choosing joystick setups like Cursor or Sinclair.
int load_set()
{
  int posn = 3;

  ili9341_clear(0); // clear screen
  printx2(0, 0, "Choose Button Set");
  printx2(4, 3, "Cursor/Protek");
  printx2(4, 6, "q,a,o,p,m");
  printx2(4, 9, "Sinclair");
  printx2(4, 12, "Go Back");
  printx2(0, posn, ">");
  while (1)
  {
    odroid_input_gamepad_read(&joystick);
    if (joystick.values[ODROID_INPUT_DOWN])
    {
      printx2(0, posn, " ");
      posn += 3;
      if (posn > 12)
        posn = 3;
      printx2(0, posn, ">");
      debounce(ODROID_INPUT_DOWN);
    }
    if (joystick.values[ODROID_INPUT_UP])
    {
      printx2(0, posn, " ");
      posn -= 3;
      if (posn < 3)
        posn = 12;
      printx2(0, posn, ">");
      debounce(ODROID_INPUT_UP);
    }
    if (joystick.values[ODROID_INPUT_A])
    {
      debounce(ODROID_INPUT_A);
      if (posn == 3)
      {
        b_up = 6;
        b_down = 5;
        b_left = 4;
        b_right = 7;
        b_a = 9;
        return (0);
      }
      if (posn == 6)
      {
        b_up = 10;
        b_down = 20;
        b_left = 18;
        b_right = 19;
        b_a = 37;
        return (0);
      }
      if (posn == 9)
      {
        b_up = 8;
        b_down = 7;
        b_left = 5;
        b_right = 6;
        b_a = 9;
        return (0);
      }
      if (posn == 12)
        return (0);
    }
    if (joystick.values[ODROID_INPUT_MENU])
    {
      debounce(ODROID_INPUT_MENU);
      return (0);
    }
  }
}

//-----------------------------------------------------------------
int setup_buttons()
{
  int posn = 0, redraw = 0;

  while (1)
  {
    ili9341_clear(0); // clear screen

    printx2(4, 0, "Up     = ");
    printx2(22, 0, key[b_up]);
    printx2(4, 3, "Down   = ");
    printx2(22, 3, key[b_down]);
    printx2(4, 6, "Left   = ");
    printx2(22, 6, key[b_left]);
    printx2(4, 9, "Right  = ");
    printx2(22, 9, key[b_right]);
    printx2(4, 12, "A      = ");
    printx2(22, 12, key[b_a]);
    printx2(4, 15, "B      = ");
    printx2(22, 15, key[b_b]);
    printx2(4, 18, "Select = ");
    printx2(22, 18, key[b_select]);
    printx2(4, 21, "Start  = ");
    printx2(22, 21, key[b_start]);
    printx2(4, 24, "Choose a Set");
    printx2(4, 27, "Done.");
    printx2(0, posn, ">");
    redraw = 0;
    while (!redraw)
    {
      odroid_input_gamepad_read(&joystick);
      if (joystick.values[ODROID_INPUT_DOWN])
      {
        printx2(0, posn, " ");
        posn += 3;
        if (posn > 27)
          posn = 0;
        printx2(0, posn, ">");
        debounce(ODROID_INPUT_DOWN);
      }
      if (joystick.values[ODROID_INPUT_UP])
      {
        printx2(0, posn, " ");
        posn -= 3;
        if (posn < 0)
          posn = 27;
        printx2(0, posn, ">");
        debounce(ODROID_INPUT_UP);
      }
      if (joystick.values[ODROID_INPUT_A])
      {
        debounce(ODROID_INPUT_A);
        if (posn == 0)
        {
          b_up = get_key(b_up);
          redraw = 1;
        }
        if (posn == 3)
        {
          b_down = get_key(b_down);
          redraw = 1;
        }
        if (posn == 6)
        {
          b_left = get_key(b_left);
          redraw = 1;
        }
        if (posn == 9)
        {
          b_right = get_key(b_right);
          redraw = 1;
        }
        if (posn == 12)
        {
          b_a = get_key(b_a);
          redraw = 1;
        }
        if (posn == 15)
        {
          b_b = get_key(b_b);
          redraw = 1;
        }
        if (posn == 18)
        {
          b_select = get_key(b_select);
          redraw = 1;
        }
        if (posn == 21)
        {
          b_start = get_key(b_start);
          redraw = 1;
        }
        if (posn == 24)
        {
          load_set();
          redraw = 1;
        }
        if (posn == 27)
          return (0);
      }
      if (joystick.values[ODROID_INPUT_MENU])
      {
        debounce(ODROID_INPUT_MENU);
        return (0);
      }
    }
  }
}

//----------------------------------------------------------------
int menu()
{
  int posn;
  odroid_volume_level level;
  char s[80];

  level = odroid_audio_volume_get();
  odroid_audio_volume_set(0); // turn off sound when in menu...
  process_sound();

  lastborder = 100; // force border re-draw after menu exit....

  //debounce inital menu button press first....
  odroid_input_gamepad_read(&joystick);
  debounce(ODROID_INPUT_MENU);
  keyboard = 0;     // make sure virtual keyboard switched off now
  ili9341_clear(0); // clear screen
  printx2(0, 0, "ZX Spectrum Emulator");
  printx2(4, 6, "Keyboard");
  printx2(4, 9, "Load Snapshot");
  printx2(4, 12, "Save Snapshot");
  printx2(4, 15, "Setup Buttons");
  printx2(4, 18, "Toggle Turbo Mode");
  printx2(4, 21, "Back To Emulator");

  odroid_input_battery_level_read(&battery_state);
  sprintf(s, "Battery: %i%%", battery_state.percentage);
  printx2(0, 28, s);
  posn = 6;
  printx2(0, posn, ">");
  while (1)
  {
    odroid_input_gamepad_read(&joystick);
    if (joystick.values[ODROID_INPUT_DOWN])
    {
      printx2(0, posn, " ");
      posn += 3;
      if (posn > 21)
        posn = 6;
      printx2(0, posn, ">");
      debounce(ODROID_INPUT_DOWN);
    }
    if (joystick.values[ODROID_INPUT_UP])
    {
      printx2(0, posn, " ");
      posn -= 3;
      if (posn < 6)
        posn = 21;
      printx2(0, posn, ">");
      debounce(ODROID_INPUT_UP);
    }
    if (joystick.values[ODROID_INPUT_A])
    {
      debounce(ODROID_INPUT_A);
      if (posn == 6)
      {
        keyboard = 1;
        draw_keyboard();
      }
      if (posn == 9)
        choose_file();
      if (posn == 12)
        save();
      if (posn == 15)
      { // set up buttons, save back new settings
        setup_buttons();
        save_buttons();
      }
      if (posn == 18)
        sp_nosync = !sp_nosync;

      //      if (posn==21) {
      //        odroid_system_application_set(0); // set menu slot
      //	esp_restart(); // reboot!
      //      }

      odroid_audio_volume_set(level); // restore sound...
      return (0);
    }
    if (joystick.values[ODROID_INPUT_MENU])
    {
      debounce(ODROID_INPUT_MENU);
      odroid_audio_volume_set(level); // restore sound...
      return (0);
    }
  }
}

//----------------------------------------------------------------
void app_main(void)
{
  printf("odroid start.\n");

  nvs_flash_init();
  odroid_system_init();
  odroid_input_gamepad_init();

  // Boot state overrides
  bool forceConsoleReset = false;

  switch (esp_sleep_get_wakeup_cause())
  {
  case ESP_SLEEP_WAKEUP_EXT0:
  {
    printf("app_main: ESP_SLEEP_WAKEUP_EXT0 deep sleep wake\n");
    break;
  }

  case ESP_SLEEP_WAKEUP_EXT1:
  case ESP_SLEEP_WAKEUP_TIMER:
  case ESP_SLEEP_WAKEUP_TOUCHPAD:
  case ESP_SLEEP_WAKEUP_ULP:
  case ESP_SLEEP_WAKEUP_UNDEFINED:
  {
    printf("app_main: Non deep sleep startup\n");

    odroid_gamepad_state bootState = odroid_input_read_raw();

    if (bootState.values[ODROID_INPUT_MENU])
    {
      // Force return to factory app to recover from
      // ROM loading crashes

      // Set factory app
      const esp_partition_t *partition = esp_partition_find_first(ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_FACTORY, NULL);
      if (partition == NULL)
      {
        abort();
      }

      esp_err_t err = esp_ota_set_boot_partition(partition);
      if (err != ESP_OK)
      {
        abort();
      }

      // Reset
      esp_restart();
    }

    if (bootState.values[ODROID_INPUT_START])
    {
      // Reset emulator if button held at startup to
      // override save state
      forceConsoleReset = true;
    }

    break;
  }
  default:
    printf("app_main: Not a deep sleep reset\n");
    break;
  }

  // allocate a screen buffer...
  buffer = (unsigned short *)malloc(16384);

  // Display
  ili9341_prepare();
  ili9341_init();

  odroid_input_battery_level_init();

  odroid_sdcard_open("/sd"); // map SD card.

  // see if there is a 'resume.txt' file, use it if so...
  read_resume();

  // Audio hardware
  odroid_audio_init(AUDIO_SAMPLE_RATE);

  // Start Spectrum emulation here.......
  sp_init();
  load_spectrum_files(0); // load snapshot and button mapping, no need to save the resume.txt file
  start_spectemu();
}
