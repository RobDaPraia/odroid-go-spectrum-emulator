# Changelog


# 20190113

- Store a set of button mappings per game. For a game [gamename].z80 the button mappings are stored in [gamename].map

- resume.txt no longer contains the button mappings, only the file and the path.

- Save game in separate file, so the original ROM stays unmodified and can be reloaded. For a game [gamename].z80 the game is saved as [gamename]-sav.z80

- Show the list of games alphabetically

- Only show directories, *.z80 and *.sna files in the 'Load Snapshot' feature


# 20180113

- First release
