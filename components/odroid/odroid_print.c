#include "esp_heap_caps.h"
#include "string.h"
#include "stdio.h" 

#include "../spectrum/rom_imag.c"
#include "odroid_display.h"

//----------------------------------------------------------------
void print(short x, short y, char *s)
{
  int rompos, i, n, k, a, len, idx;
  char c;
  extern unsigned short *buffer; // screen buffer, 16 bit pixels

  len = strlen(s);
  for (k = 0; k < len; k++)
  {
    c = s[k];
    rompos = 15616; // start of character set
    rompos += c * 8 - 256;

    for (i = 0; i < 8; i++)
    {
      a = rom_imag[rompos++];
      for (n = 7, idx = i * len * 8 + k * 8 + 7; n >= 0; n--, idx--)
      {
        if (a % 2 == 1)
          buffer[idx] = 65535;
        else
          buffer[idx] = 0;
        a = a >> 1;
      }
    }
  }
  ili9341_write_frame_rectangle(x * 8, y * 8, len * 8, 8, buffer);
}

//----------------------------------------------------------------
void printx2(short x, short y, char *s)
{
  int rompos, i, n, k, a, len, idx;
  char c;
  extern unsigned short *buffer; // screen buffer, 16 bit pixels

  len = strlen(s);
  for (k = 0; k < len; k++)
  {
    c = s[k];
    rompos = 15616; // start of character set
    rompos += c * 8 - 256;

    for (i = 0; i < 8; i++)
    {
      a = rom_imag[rompos++];
      for (n = 7, idx = i * len * 32 + k * 16 + 14; n >= 0; n--, idx -= 2)
      {
        if (a % 2 == 1)
        {
          buffer[idx] = -1;
          buffer[idx + 1] = -1;
          buffer[idx + len * 16] = -1;
          buffer[idx + len * 16 + 1] = -1;
        }
        else
        {
          buffer[idx] = 0;
          buffer[idx + 1] = 0;
          buffer[idx + len * 16] = 0;
          buffer[idx + len * 16 + 1] = 0;
        }
        a = a >> 1;
      }
    }
  }
  ili9341_write_frame_rectangle(x * 8, y * 8, len * 16, 16, buffer);
}
