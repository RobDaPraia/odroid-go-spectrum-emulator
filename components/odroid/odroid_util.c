#include "esp_heap_caps.h"

#include "../spectrum/misc.h"

#include "string.h"
#include "stdio.h"
#include "ctype.h"
#include "sys/stat.h"

// take care of freeing memory allocated by malloc();
// https://stackoverflow.com/questions/29655051/proper-way-to-free-memory-of-a-returned-variable

char *odroid_util_GetFileName(const char *path)
{
	int length = strlen(path);
	int fileNameStart = length;

	if (fileNameStart < 1)
	{
		printf("odroid_util_GetFileName: Error: path is empty\n");
		abort();
	}

	while (fileNameStart > 0)
	{
		if (path[fileNameStart] == '/')
		{
			++fileNameStart;
			break;
		}

		--fileNameStart;
	}

	int size = length - fileNameStart + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	result[size - 1] = 0;
	for (int i = 0; i < size - 1; ++i)
	{
		result[i] = path[fileNameStart + i];
	}

	//printf("odroid_util_GetFileName: result='%s'\n", result);

	return result;
}

char *odroid_util_GetFileExtension(const char *path)
{
	// Note: includes '.'
	int length = strlen(path);
	int extensionStart = length;

	if (extensionStart < 1)
	{
		printf("odroid_util_GetFileExtension: Error: path is empty\n");
		abort();
	}

	while (extensionStart > 0)
	{
		if (path[extensionStart] == '.')
		{
			break;
		}

		--extensionStart;
	}

	int size = length - extensionStart + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	result[size - 1] = 0;
	for (int i = 0; i < size - 1; ++i)
	{
		result[i] = path[extensionStart + i];
	}

	//printf("odroid_util_GetFileExtension: result='%s'\n", result);

	return result;
}

char *odroid_util_GetPathWithoutExtension(const char *path)
{
	int length = strlen(path);
	int extensionStart = length;

	if (extensionStart < 1)
	{
		printf("odroid_util_GetPathWithoutExtension: Error: path is empty\n");
		abort();
	}

	while (extensionStart > 0)
	{
		if (path[extensionStart] == '.')
		{
			break;
		}

		--extensionStart;
	}

	int size = extensionStart + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	result[size - 1] = 0;
	for (int i = 0; i < size - 1; ++i)
	{
		result[i] = path[i];
	}

	//printf("odroid_util_GetPathWithoutExtension: result='%s'\n", result);

	return result;
}

char *odroid_util_GetFileNameWithoutExtension(const char *path)
{
	char *fileName = odroid_util_GetFileName(path);

	fileName = odroid_util_GetPathWithoutExtension(fileName);

	int size = strlen(fileName) + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	strcpy(result, fileName);
	free_string(fileName);

	//printf("odroid_util_GetFileNameWithoutExtension: result='%s'\n", result);

	return result;
}

char *odroid_util_GetCleanPathWithoutExtension(const char *path)
{
	char *path2 = odroid_util_GetPathWithoutExtension(path);

	// do we have a [gamename].z80 or [gamename]-sav.z80 path?
	int length = strlen(path2);
	if (length > 4)
	{
		const char *last_four = &path2[length - 4];

		if (strcasecmp("-sav", last_four) == 0)
		{
			//printf("odroid_util_GetCleanPathWithoutExtension: found '-sav' in path '%s'\n", path2);

			path2[length - 4] = 0;
			length -= 4;
		}
	}

	int size = length + 1;
	char *result = malloc(size);
	if (!result)
		abort();

	strcpy(result, path2);
	free_string(path2);

	//printf("odroid_util_GetCleanPathWithoutExtension: result='%s'\n", result);

	return result;
}

char *odroid_util_GetCleanFileNameWithoutExtension(const char *path)
{
	char *fileName = odroid_util_GetFileName(path);

	fileName = odroid_util_GetCleanPathWithoutExtension(fileName);

	int size = strlen(fileName) + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	strcpy(result, fileName);
	free_string(fileName);

	//printf("odroid_util_GetCleanFileNameWithoutExtension: result='%s'\n", result);

	return result;
}

char *odroid_util_GetSavePath(const char *path)
{
	char *fileExtension = odroid_util_GetFileExtension(path);
	char *path2 = odroid_util_GetCleanPathWithoutExtension(path);

	int length = strlen(path2);
	int size = length + 4 + 4 + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	result[0] = 0;
	strcat(result, path2);
	strcat(result, "-sav");
	strcat(result, fileExtension);

	free_string(fileExtension);
	free_string(path2);

	//printf("odroid_util_GetSavePath: result='%s'\n", result);

	return result;
}

//---------------------------------------------------------------
int odroid_util_IsRomFile(const char *path)
{
	if (strlen(path) < 1)
	{
		printf("odroid_util_IsRomFile: false, empty path\n");
		return 0;
	}

	int result = 0;
	char *fileExtension = odroid_util_GetFileExtension(path);

	if ((strcasecmp(fileExtension, ".z80") == 0) || (strcasecmp(fileExtension, ".sna") == 0))
	{
		//printf("odroid_util_IsRomFile: true, '%s' with extension '%s'\n", path, fileExtension);
		result = 1;
	}
	else
	{
		//printf("odroid_util_IsRomFile: false, '%s' with extension '%s'\n", path, fileExtension);
	}

	free_string(fileExtension);

	return result;
}

//---------------------------------------------------------------
char *odroid_util_GetButtonsPath(const char *file)
{

	if (strlen(file) == 0)
	{
		printf("odroid_util_GetButtonsPath: warning: file is empty string\n");
		strcpy(file, "");
		return file;
	}
	if (!odroid_util_IsRomFile(file))
	{
		printf("odroid_util_GetButtonsPath: warning file '%s' is not a rom file: \n", file);
		strcpy(file, "");
		return file;
	}
	//printf("odroid_util_GetButtonsPath: '%s'\n", file);

	char *fileName = odroid_util_GetCleanPathWithoutExtension(file);

	int size = strlen(fileName) + 4 + 1;

	char *result = malloc(size);
	if (!result)
		abort();

	result[size - 1] = 0;

	for (int i = 0; i < size - 1; ++i)
	{
		result[i] = fileName[i];
	}

	strcat(result, ".map");

	//printf("odroid_util_GetButtonsPath: result='%s'\n", result);

	free_string(fileName);
	return result;
}

