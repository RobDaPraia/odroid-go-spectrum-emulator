#pragma once

#include <stdint.h>

char* odroid_util_GetFileName(const char* path);
char* odroid_util_GetFileExtension(const char* path);
char* odroid_util_GetFileNameWithoutExtension(const char* path);
char *odroid_util_GetCleanPathWithoutExtension(const char *path);
char *odroid_util_GetCleanFileNameWithoutExtension(const char *path);
char* odroid_util_GetPathWithoutExtension(const char* path);
int odroid_util_IsRomFile(const char *path);
char *odroid_util_GetButtonsPath(const char *file);
char *odroid_util_GetSavePath(const char *path);
//int odroid_util_ScanRoms(const char *dirname);