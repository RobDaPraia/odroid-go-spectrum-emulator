#include "odroid_util.h"

#include "../spectrum/misc.h"

#include "string.h"
#include "stdio.h"
#include "ctype.h"
#include "sys/stat.h"

#include <dirent.h>
#include <sys/types.h>
#include <stdlib.h>

static int odroid_alphasort(const void *p, const void *q)
{
    // https://stackoverflow.com/questions/10443132/using-qsort-to-sort-pointers-to-structs-containing-strings

    // pn is a pointer to an element of the array,
    // so, it's effectively a pointer to a pointer to a struct.
    // Therefore, we need to cast it appropriately to struct student **.
    // To get a pointer to a struct from it, we dereference it once,
    // hence the "*". Then we need to extract a pointer to the beginning
    // of a string, hence the "->".
    // return strcmp((*(struct student **)p)->d_name,
    //               (*(struct student **)q)->d_name);

    const char *l = (*(struct dirent **)p)->d_name;
    const char *r = (*(struct dirent **)q)->d_name;

    int cmp = strcasecmp(l, r);

    //printf("odroid_Scandir: odroid_alphasort, l='%s', r='%s', cmp=%i\n", l, r,cmp);

    return cmp;
}

int odroid_Scandir(const char *path, struct dirent ***namelist)
{
    // https://github.com/msysgit/msys/blob/master/newlib/libc/posix/scandir.c
    // https://www.linuxquestions.org/questions/programming-9/how-to-list-and-sort-files-in-some-directory-by-the-names-on-linux-win-in-c-4175555160/
    int n = 0;
    DIR *dr;
    struct dirent *de, *p, **names;
    int size_dirent = sizeof(struct dirent);
    //printf("odroid_Scandir: size_dirent: '%i',\n", size_dirent); // 264

    dr = opendir(path);
    if (dr == NULL) // opendir returns NULL if couldn't open directory
    {
        printf("odroid_Scandir: warning: Could not open directory '%s'\n", path);
        return 0;
    }

    int arraysz = 250; // for simplicitly max 250
    names = malloc(arraysz * sizeof(struct dirent *));
    if (names == NULL)
        return (-1);

    //printf("odroid_Scandir: scanning: '%s'\n", path);

    while ((de = readdir(dr)) != NULL && n < 250)
    {
        //printf("odroid_Scandir: n='%i', read file: '%s', lenght: '%i',\n", n, de->d_name, strlen(de->d_name));

        if (de->d_type == DT_DIR || odroid_util_IsRomFile(de->d_name) == 1)
        {
            p = malloc(size_dirent);
            if (p == NULL)
                return (-1);

            p->d_ino = de->d_ino;
            p->d_type = de->d_type;
            strcpy(p->d_name, de->d_name);
            names[n] = p;
            n++;
        }
    }
    closedir(dr);

    int result = n;
    if (n > 1)
    {
        qsort(names, n, sizeof(struct dirent *), odroid_alphasort);
    }
    *namelist = names;

    //printf("odroid_Scandir: returning result= %i\n", result);
    return result;
}
