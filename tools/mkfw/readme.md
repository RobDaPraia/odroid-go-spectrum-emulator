# Windows mkfw tool

To create a spectrum.fw file, copy the spectrum.bin to this directory and run 2fw.cmd

The mkfw tool was created by compiling the source code from [github OtherCrashOverride/odroid-go-firmware](https://github.com/OtherCrashOverride/odroid-go-firmware/tree/master/tools/mkfw)

On Windows you can download the [Tiny C Compiler](https://bellard.org/tcc/) to build mkfw

* Create a directory for the tcc, for example E:\Tools\tcc\
* Add E:\tools\tcc to your path
* clone the [odroid firmware](https://github.com/othercrashoverride/odroid-go-firmware.git), for example in D:\dev\odroid\odroid-go-firmware
* open console and enter the following commands:

```
D:
cd D:\dev\odroid\odroid-go-firmware\tools\mkfw
D:\dev\odroid\odroid-go-firmware\tools\mkfw>tcc -g main.c crc32.c -o mkfw.exe
```

small test of the created mkfw application:

```
D:\dev\odroid\odroid-go-firmware\tools\mkfw>mkfw
usage: mkfw description tile type subtype length label binary [...]
```
