REM https://forum.odroid.com/viewtopic.php?f=159&t=31314

REM Simple 2fw.cmd to simplify life
REM Using 2fw myapp.bin
REM myapp.raw must exists.


@echo off
echo.
set /a size=%~z1
set /a blocks = (size / 65536 + 1)*65536
mkfw "%~n1" %~n1.raw 0 16 %blocks% app %~n1.bin
echo.
if EXIST %~n1.fw del %~n1.fw 
ren firmware.fw %~n1.fw
echo  inFile: %1 Filesize: %size% Partition size: %blocks%
echo outFile: %~n1.fw

pause