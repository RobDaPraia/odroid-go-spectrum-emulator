# Build on Windows

__Todo__: add documentation how to build on windows from scratch, set-up tools, download sdks, fork repro/pull request


# Windows mkfw tool

You can create a spectrum.fwd file using the mkfw tool in the directory \odroid-go-firmware\tools\mkfw

To create the spectrum.fw file:

* copy the spectrum.bin to the directory \odroid-go-firmware\tools\mkfw
* run \odroid-go-firmware\tools\mkfw\make_spectrum.cmd

Note: the mkfw tool was compiled from source code at [github OtherCrashOverride/odroid-go-firmware](https://github.com/OtherCrashOverride/odroid-go-firmware/tree/master/tools/mkfw) using [Tiny C Compiler](https://bellard.org/tcc/). See [fwd compile instructions](./tools/mkfw/readme.md)